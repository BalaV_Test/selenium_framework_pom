package com.qa.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedHashMap;
import java.util.Map;


import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;

public class ExcelUtil extends TestBase {
	
	
	public static Object[][] data(String sheetname) throws Throwable {

		
		File f = new File(ConstantUtil.testdatapath);
		FileInputStream st = new FileInputStream(f);
		@SuppressWarnings("resource")
		Workbook w = new XSSFWorkbook(st);
		Sheet sheet = w.getSheet(sheetname);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] obj = new Object[lastRowNum][1];

		for (int i = 0; i < lastRowNum; i++) {
			Map<String, String> datamap = new LinkedHashMap<String, String>();

			for (int j = 0; j < lastCellNum; j++) {
				String key = sheet.getRow(0).getCell(j).toString();
				String value = sheet.getRow(i + 1).getCell(j).toString();
				datamap.put(key, value);
			}
			obj[i][0] = datamap;
			

		}
		return obj;

	}


}
