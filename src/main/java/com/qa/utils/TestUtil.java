package com.qa.utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.io.FileHandler;


import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;

public class TestUtil extends TestBase {
public static long PAGE_LOAD_TIMEOUT=30;
public static long IMPLICIT_WAIT=10;
 //public static String TESTDATA_SHEET_PATH="C:\\Users\\bala\\eclipse-workspace\\CRMTest\\src\\main\\java\\com\\qa\\testdata\\CRMTestData.xlsx";
 
static Workbook book;
static Sheet sheet;

public static Object[][] getTestData(String sheetname){
	FileInputStream file=null;
	try {
		file=new FileInputStream(ConstantUtil.testdatapath);
		
	}catch(FileNotFoundException e) {
		e.printStackTrace();
	}
	try {
		book=WorkbookFactory.create(file);
	}catch(IOException e) {
		e.printStackTrace();
	}
	sheet=book.getSheet(sheetname);
	Object[][] data=new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
	
	for(int i=0;i<sheet.getLastRowNum();i++) {
		for(int j=0;j<sheet.getRow(0).getLastCellNum();j++) {
			data[i][j]=sheet.getRow(i+1).getCell(j).toString();
		}
	}
	return data;
}

//public static void takeScreenshot() throws IOException {
	//File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	//FileHandler.copy(src, new File("C:\\Users\\bala\\Desktop\\Screenshot\\"+System.currentTimeMillis()+"---screenshot.png"));
//}

public static void takeScreenshot(String fileName,String filePath)  {
	
    try {
    	File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	FileHandler.copy(src, new File(ConstantUtil.screenshotpath+filePath+"\\"+fileName+System.currentTimeMillis()+"_screenshot.png"));
        
    } catch (Exception e) {
          e.printStackTrace();
    } 
    }
}














