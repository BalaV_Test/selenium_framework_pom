package com.qa.config;

public interface ConstantUtil {
	
	
		 //Constant Variables are declared a final, 
	//so that they cannot be changed during the execution.

	        public static final String loginfoldername = "login";
	        
	        public static final String usercreationfolder = "usercreation";
	        
	        public static final String companycreationfolder = "companycreation";
	        
	        public static long  PAGE_LOAD_TIMEOUT=30;
	        
	        public static long  IMPLICIT_WAIT=10;
	        
	        public static final String testdatapath="C:\\Users\\bala\\eclipse-workspace\\CRMTest\\src\\main\\java\\com\\qa\\testdata\\CRMTestData.xlsx";
	        
	        public static final String screenshotpath="C:\\Users\\bala\\Desktop\\Screenshot\\";

	        

}
