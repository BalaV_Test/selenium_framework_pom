package com.qa.base;

import java.util.Properties;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.util.Properties;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.qa.config.ConfigFileReader;
import com.qa.config.ConstantUtil;
//import com.qa.utils.TestUtil;

public class TestBase  {
public static WebDriver driver;
public static Properties prop;
public static ConfigFileReader configFileReader= new ConfigFileReader();
//public TestBase() {
//	try {
//		prop=new Properties();
//		FileInputStream ip=new FileInputStream("C:\\Users\\bala\\eclipse-workspace\\CRMTest\\src\\main\\java\\com\\qa\\config\\config.properties");
//		prop.load(ip);
//	}
//	catch(FileNotFoundException e){
//		e.printStackTrace();
//		
//	}
//	catch(IOException e) {
//		e.printStackTrace();
//	}
//
//}
public static void initialization() {
	//String browserName=prop.getProperty("browser");
	//String browserName=ConstantUtil.browser;
	//String browserPath=ConstantUtil.browserpath;
	String browserName=configFileReader.getBrowserName();
	String browserPath=configFileReader.getBrowserPath();
	if(browserName.equals("chrome")) {	
	System.setProperty("webdriver.chrome.driver",browserPath);
	driver=new ChromeDriver();
	}
driver.manage().window().maximize();
driver.manage().deleteAllCookies();
driver.manage().timeouts().pageLoadTimeout(ConstantUtil.PAGE_LOAD_TIMEOUT,TimeUnit.SECONDS);
driver.manage().timeouts().implicitlyWait(ConstantUtil.IMPLICIT_WAIT,TimeUnit.SECONDS);
//driver.get(prop.getProperty("url"));
//driver.get(ConstantUtil.url);
driver.get(configFileReader.getApplicationUrl());	
}
}