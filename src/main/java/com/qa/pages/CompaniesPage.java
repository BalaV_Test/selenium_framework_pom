package com.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;
import com.qa.utils.TestUtil;

public class CompaniesPage extends TestBase {
	
	
@FindBy(xpath="//*[@id=\"dashboard-toolbar\"]/div[2]/div/a/button")
private WebElement newbutton;

@FindBy(name="name")
private WebElement companyName;

@FindBy(xpath="//*[@id=\"dashboard-toolbar\"]/div[2]/div/button[2]")
private WebElement savebutton;

public CompaniesPage() {
	 PageFactory.initElements(driver,this);
	 
}

public void clickOnNew() {
	newbutton.click();
		
}
public CompaniesPage newCompanyCreation(String compname) throws IOException {
	companyName.sendKeys(compname);
	savebutton.click();
	TestUtil.takeScreenshot(compname,ConstantUtil.companycreationfolder);
	return new CompaniesPage();
	
}

}
