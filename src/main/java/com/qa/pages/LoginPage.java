package com.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//import com.qa.base.PageBase;
import com.qa.base.TestBase;
import com.qa.utils.TestUtil;

public class LoginPage extends TestBase{

	
	//PageFactory acts as Object  Repository
	@FindBy(name="email")
private	WebElement username;
	
	@FindBy(name="password")
private	WebElement password;
	
	@FindBy(xpath="//*[@id=\"ui\"]/div/div/form/div/div[3]") //"button[contains(text(),'Login')]"
private	WebElement  loginButton;
	
	@FindBy(linkText="Sign Up")
private	WebElement signupButton;
	
//To Initialize the PageObjects	
	public LoginPage() {
		 PageFactory.initElements(driver,this);
		 
	}
//Actions to be performed,by using the page objects
	
	public String validateLoginPageTitle() {
		return driver.getTitle();
	}
	public HomePage login(String un,String pwd,String log) throws IOException{
		
		username.sendKeys(un);
		password.sendKeys(pwd);
		loginButton.click();
		TestUtil.takeScreenshot(un,log);
		return new HomePage();
		
	}
	
	
	
	
	

}
 