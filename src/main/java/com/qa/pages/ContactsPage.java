package com.qa.pages;



import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;
import com.qa.utils.TestUtil;


public class ContactsPage extends TestBase {
	
	
@FindBy(xpath="//*[@id=\"dashboard-toolbar\"]/div[2]/div/a/button")
private WebElement newbutton;

@FindBy(name="first_name")
private WebElement firstName;

@FindBy(name="last_name")
private WebElement lastName;

@FindBy(xpath="//*[@id=\"dashboard-toolbar\"]/div[2]/div/button[2]")
private WebElement savebutton;

public ContactsPage() {
	 PageFactory.initElements(driver,this);
	 
}

public void clickOnNew() {
	newbutton.click();
		
}
public ContactsPage newUserCreation(String fname,String lname) throws IOException {
	
	firstName.sendKeys(fname);
	lastName.sendKeys(lname);
	savebutton.click();
	TestUtil.takeScreenshot(fname+lname,ConstantUtil.usercreationfolder);
	return new ContactsPage();
	
}


}
