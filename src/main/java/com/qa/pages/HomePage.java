package com.qa.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.TestBase;


public class HomePage extends TestBase {

@FindBy(xpath="//input[@class='user-display']")
private WebElement userLabel;

@FindBy(xpath="//*[@id=\"main-nav\"]/a[3]")
private WebElement Contacts;

@FindBy(xpath="//*[@id=\"main-nav\"]/div[4]/a/span")
private WebElement Companies;

@FindBy(xpath="//a[contains(text(),'Calendar')]")
private WebElement Calendar;

@FindBy(xpath="//a[contains(text(),'Tasks')]")
private WebElement Tasks;

public HomePage() {
	PageFactory.initElements(driver,this);
}

public String verifyHomePageTitle() {
	return driver.getTitle();
}


public ContactsPage clickOnContacts() throws IOException {
	Contacts.click();
	return new ContactsPage();	
}
public CompaniesPage clickOnCompanies() {
	Companies.click();
	return new CompaniesPage();	
}

}
