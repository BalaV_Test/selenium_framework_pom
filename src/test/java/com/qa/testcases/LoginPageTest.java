package com.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

public class LoginPageTest extends TestBase {
	 LoginPage loginPage;
	 HomePage homePage;
	 Logger log=Logger.getLogger(LoginPageTest.class);
//	public LoginPageTest() {
//		super();//calling super class constructor(ie.TestBase)
//	}
	@BeforeMethod
	public void setUp() {
		initialization();
		loginPage=new LoginPage();
	}
	@Test(priority=1)
	public void loginPageTitleTest() {
		log.info("*****Test1:Title Verification******");
		String title=loginPage.validateLoginPageTitle();
		Assert.assertEquals(title,"Cogmento CRM");
	}
	@Test(priority=2)
	public void loginTest() throws IOException {
		log.info("*****Test2:Logging into FreeCRM Website******");
	homePage=loginPage.login(configFileReader.getUserName(),configFileReader.getPassword(),ConstantUtil.loginfoldername);
		//login method is returning homepage
	} 
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
