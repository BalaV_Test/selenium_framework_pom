package com.qa.testcases;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;
import com.qa.pages.ContactsPage;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

import com.qa.utils.TestUtil;



public class ContactsPageTest extends TestBase {

	LoginPage loginPage;
	HomePage homePage;
	ContactsPage contactsPage;
	String sheetName="Contacts";
	
//	public ContactsPageTest() {
//		super();//Base Class Constructor will be called for properties  
//		}
	@BeforeMethod
	public void setUp() throws IOException {
		initialization();
		loginPage=new LoginPage();
		homePage=loginPage.login(configFileReader.getUserName(),configFileReader.getPassword(),ConstantUtil.loginfoldername);
		contactsPage=homePage.clickOnContacts();
	}
	

	@Test(priority=1,dataProvider="getData")
	public void newUserCreationTest(String firstname,String lastname) throws IOException  {
		contactsPage.clickOnNew();
		contactsPage=contactsPage.newUserCreation(firstname,lastname);
			
	} 
	
	@DataProvider
	public Object[][] getData(){
		Object data[][]=TestUtil.getTestData(sheetName);
		return data;
	
		
	}
	
	
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
		
	}
}
