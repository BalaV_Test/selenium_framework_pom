package com.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.config.ConstantUtil;
import com.qa.pages.ContactsPage;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;

public class HomePageTest extends TestBase{
LoginPage loginPage;
HomePage homePage;
ContactsPage contactsPage;
//	public HomePageTest() {
//		super();//Base Class Constructor will be called for properties  
//	}
@BeforeMethod
public void setUp() throws IOException {
	initialization();
	loginPage=new LoginPage();
	homePage=loginPage.login(configFileReader.getUserName(),configFileReader.getPassword(),ConstantUtil.loginfoldername);
}
@Test(priority=1)
public void verifyHomePageTitleTest() {
	String homeTitle=homePage.verifyHomePageTitle();
	Assert.assertEquals(homeTitle,"Cogmento CRM","HomePageTitle Match Failed");
	//3rd parameter will be printed only when the test fails
	
}

@Test(priority=2)
public void verifyContactsTest() throws IOException {
	contactsPage=homePage.clickOnContacts();
	
}

@AfterMethod
public void tearDown() {
	driver.quit();
}
}
