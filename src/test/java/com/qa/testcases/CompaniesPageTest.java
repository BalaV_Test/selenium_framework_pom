package com.qa.testcases;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.config.ConfigFileReader;
import com.qa.config.ConstantUtil;
import com.qa.pages.CompaniesPage;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.utils.ExcelUtil;




public class CompaniesPageTest extends TestBase {
	LoginPage loginPage;
	HomePage homePage;
	CompaniesPage company;
	ConfigFileReader configFileReader= new ConfigFileReader();
	String sheetName="Companies";
	
	
	
	public CompaniesPageTest() {
		super();//Base Class Constructor will be called for properties  
		}
	@BeforeMethod
	public void setUp() throws IOException {
		initialization();
		loginPage=new LoginPage();
		homePage=loginPage.login(configFileReader.getUserName(),configFileReader.getPassword(),ConstantUtil.loginfoldername);
		company=homePage.clickOnCompanies();
	}

	@Test(priority=1,dataProvider="getData")
	public  void tc1(Map<String,String> map) throws IOException  {
		company.clickOnNew();
		company=company.newCompanyCreation(map.get("Company"));
			
	} 
	
	

	@DataProvider
	public Object[][] getData() throws Throwable{
		
		Object data[][]=ExcelUtil.data(sheetName);
		return data;
	
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
		
	}
}
